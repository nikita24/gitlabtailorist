<?php

class OnlineBizsoft_Reseller_Model_Observer extends Varien_Event_Observer {

    public function handleAdminUserSaveAfter($observer) {
        $editor = Mage::getSingleton('admin/session')->getUser();
        if (!$editor) // API or smth else
            return $this;

        $user = $observer->getDataObject();
        if ($editor->getId() == $user->getId()) { // My Account
            return $this;
        }
        $ids = $user->getSelectedMembers();
        if (is_null($ids))
            return $this;
        $ids = Mage::helper('adminhtml/js')->decodeGridSerializedInput($ids);
        Mage::getModel('reseller/reseller')->assignMembers($user->getId(), $ids);

        return $this;
    }

    public function createCommissionForInvoice($observer) {
        $invoice = $observer->getEvent()->getInvoice();
		global $orderIdCustom;
        $order = $invoice->getOrder();
        $order_id = $invoice->getOrderId();
		$orderIdCustom = $invoice->getOrderId();
		
        $increment_id = $order->getIncrementId();
        $helper = Mage::helper('reseller');
        $uid = Mage::getModel('amperm/perm')->getUserByOrder($order_id);
        $user = Mage::getModel('admin/user')->load($uid);
        $total_amount = $order->getBaseGrandTotal();
        $data = array(
            'entity_id' => $invoice->getIncrementId(),
            'entity_type' => OnlineBizsoft_Reseller_Model_Commission::TYPE_INVOICE,
            'total_amount' => $total_amount,
            'increment_id' => $increment_id,
            'created_at' => date('Y-m-d H:i:s'),
            'currency_code' => Mage::app()->getStore()->getBaseCurrencyCode()
        );
		//echo $total_amount;
		//var_dump($order);
		//die;
        $is_seller = $helper->isSeller($user);
        $is_partner = $helper->isPartner($user);
        if ($is_seller) {
            $data['seller_id'] = $user->getUserId();
            $data['partner_id'] = $user->getPartnerId();
        } elseif ($is_partner) {
            $data['partner_id'] = $user->getUserId();
        }

		global $newCityPartnerComission;
        $seller_commission_amount = null;
        $partner_commission_amount = null;
        if ($is_seller) {
            $seller_commission_amount = $helper->getCommissionAmount($user, $total_amount);
            $partner = Mage::getModel('admin/user')->load($user->getPartnerId());
            if ($partner->getId()) {
                $partner_commission_amount = $newCityPartnerComission;
            }
        } elseif ($is_partner) {
            $partner_commission_amount = $helper->getCommissionAmount($user, $total_amount);
        }
        $data['seller_commission_amount'] = $seller_commission_amount;
        $data['partner_commission_amount'] = $partner_commission_amount;
        Mage::getModel('reseller/commission')->setData($data)->save();
    }

    public function createCommissionForCreditmemo($observer) {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $order = $creditmemo->getOrder();
        $order_id = $creditmemo->getOrderId();
        $increment_id = $order->getIncrementId();
        $uid = Mage::getModel('amperm/perm')->getUserByOrder($order_id);
        $user = Mage::getModel('admin/user')->load($uid);
        $helper = Mage::helper('reseller');
        $total_amount = $order->getBaseSubtotalRefunded();
        $data = array(
            'entity_id' => $creditmemo->getIncrementId(),
            'entity_type' => OnlineBizsoft_Reseller_Model_Commission::TYPE_CREDITMEMO,
            'total_amount' => $total_amount,
            'increment_id' => $increment_id,
            'created_at' => date('Y-m-d H:i:s'),
            'currency_code' => Mage::app()->getStore()->getBaseCurrencyCode()
        );
       
        $is_seller = $helper->isSeller($user);
        $is_partner = $helper->isPartner($user);
        if ($is_seller) {
            $data['seller_id'] = $user->getUserId();
            $data['partner_id'] = $user->getPartnerId();
        } elseif ($is_partner) {
            $data['partner_id'] = $user->getUserId();
        }
        $seller_commission_amount = null;
        $partner_commission_amount = null;
        $partner = null;
        if ($is_seller) {
            $seller_commission_amount = $helper->getCommissionAmount($user, $total_amount);
            $partner = Mage::getModel('admin/user')->load($user->getPartnerId());
            if ($partner->getId()) {
                $partner_commission_amount = $helper->getCommissionAmount($partner, $total_amount, $seller_commission_amount);
            }
        } elseif ($is_partner) {
            $partner_commission_amount = $helper->getCommissionAmount($user, $total_amount);
        }
        $data['seller_commission_amount'] = 0 - $seller_commission_amount;
        $data['partner_commission_amount'] = 0 - $partner_commission_amount;
        Mage::getModel('reseller/commission')->setData($data)->save();
    }

    public function affiliateMarketingRegisterSuccess($observer) {
        $cookie = Mage::getSingleton('core/cookie');
        $affcode = $cookie->get('affcode');
        if (!$affcode) {
            return;
        }
        $user = Mage::getModel('admin/user')->load($affcode, 'affcode');
        $user_id = $user->getUserId();
        if (!$user_id) {
            return;
        }
        $customer = $observer->getEvent()->getCustomer();
        $customerId = $customer->getId();
        $userId = Mage::getModel('amperm/perm')->getResource()->getUserByCustomer($customerId);
        if (!$userId && $customerId) {
            Mage::getResourceModel('amperm/perm')->assignOneCustomer($user_id, $customerId);
        }
    }

    public function saveCustomerFromFrontend($observer) {
        $cookie = Mage::getSingleton('core/cookie');
        $affcode = $cookie->get('affcode');
        if (!$affcode) {
            return;
        }
        $user = Mage::getModel('admin/user')->load($affcode, 'affcode');
        $user_id = $user->getUserId();
        if (!$user_id) {
            return;
        }
        $this->applyResellerGroupToCustomer($observer, $user);
    }

    public function saveCustomerFromOrderCreate($observer) {
        $this->applyResellerGroupToCustomer($observer);
    }

    protected function applyResellerGroupToCustomer($observer, $user = null) {
        $helper = Mage::helper('reseller');
        $group_id = $helper->getCustomerGroup();
        $website_id = $helper->getWebsiteId();
        // getStoreId when create order on reseller
        $customer = $observer->getEvent()
                ->getCustomer();
        $store_id = $helper->getStoreId();
        if($customer->getStoreId()){
            $store_id = $customer->getStoreId();
        }
        
        $observer->getEvent()
                ->getCustomer()
                ->setStore(Mage::app()->getStore($store_id))
                ->setData('group_id', $group_id)
                ->setData('website_id', $website_id);
    }

    public function generateAffiliateCode($observer) {
        $user = $observer->getEvent()->getObject();
        if (Mage::helper('reseller')->isSalesPerson($user) && !$user->getAffcode()) {
            $resource = Mage::getSingleton('core/resource');
            $write = $resource->getConnection('core_write');
            $table = $resource->getTableName('admin/user');
            $user_id = $user->getUserId();
            $affcode = md5($user->getEmail());
            $write->query("UPDATE {$table} SET affcode='{$affcode}' WHERE user_id={$user_id}");
        }
    }

    public function addHandleOrderCreate($observer) {
        $layout = $observer->getEvent()->getLayout();
        $action = $observer->getEvent()->getAction();
        if (strpos($action->getFullActionName(), 'adminhtml_order_create') === 0 || strpos($action->getFullActionName(), 'adminhtml_checkout_cart') === 0 || strpos($action->getFullActionName(), 'adminhtml_checkout_onepage') === 0) {
            $layout->getUpdate()->addHandle('adminhtml_order_create');
        }
    }

    public function saveImageOptionToItemQuote(Varien_Event_Observer $observer) {
        $item = $observer->getQuoteItem();
        if ($item->getProductId() == 196) {
            $helper = Mage::helper('catalog/product_configuration');
            $options = $helper->getCustomOptions($item);
            foreach ($options as $option) {
                if ($option['label'] == 'Design') {
                    $data = json_decode(htmlspecialchars_decode($option['value']), true);
                    $fabric = Mage::getModel('catalog/product')->setStoreId($item->getStoreId())->loadByAttribute('sku', $data['fabric']['base']);
                    $price = $fabric->getPrice();
                    $item->setPrice($price);
                    $item->setBasePrice($price);
                    $item->setCustomPrice($price);
                    $item->setOriginalCustomPrice($price);
                    $item->calcRowTotal();
                    $item->calcTaxAmount();
                }
            }
            $images = !empty($_POST['images']) ? $_POST['images'] : '';
            if ($images) {
                $filename = 'quote/quote_item_' . time() . '.png';
                Mage::helper('designer/data')->renderImage(explode(',', $images), $filename);
                $item->addOption(array(
                    "code" => "thumb",
                    'value' => $filename
                ));
            } else {
                $item->addOption(array(
                    "code" => "thumb",
                    'value' => 'shirts/' . $fabric->getSku() . '/fabric/medium.png'
                ));
            }
            $item->save();
        }
    }

    public function salesQuoteItemSetProduct(Varien_Event_Observer $observer) {
        $item = $observer->getQuoteItem();
        if ($item->getProductId() == 196) {
            $helper = Mage::helper('catalog/product_configuration');
            $options = $helper->getCustomOptions($item);
            foreach ($options as $option) {
                if ($option['label'] == 'Design') {
                    $data = json_decode(htmlspecialchars_decode($option['value']), true);
                    $fabric = Mage::getModel('catalog/product')->setStoreId($item->getStoreId())->loadByAttribute('sku', $data['fabric']['base']);
                    $name = $fabric->getName();
                    $item->setName($name);
                    $item->setSku($fabric->getSku());
                    $item->calcRowTotal();
                    $item->calcTaxAmount();
                }
            }
        }
        return $this;
    }

    public function savePhotoForReseller($observer) {
        if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') {
            try {
                $uploader = new Varien_File_Uploader('photo');
                $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $path = Mage::getBaseDir('media') . DS . 'photo' . DS;
                $uploader->save($path, $_FILES['photo']['name']);
            } catch (Exception $e) {
                
            }
            $_POST['photo'] = 'photo/' . $_FILES['photo']['name'];
        }
    }
    
    public function initCheckoutSessionForAlipay($observer){
        $order = $observer->getOrder();
        /* @var $payment Mage_Sales_Model_Quote_Payment */
        $payment = $observer->getPayment();
        if(in_array($payment->getMethod(), array('unionpay', 'alipay', 'weixin', 'weixinmobile'))){
            Mage::register('last_real_order_id', $order->getRealOrderId());
            Mage::register('_store', Mage::helper('reseller')->getStoreId());
        }
    }
    public function initParamsAlipay($observer){
        $params = $observer->getParamsAlipay();
        if(Mage::registry('last_real_order_id')){
            $params->setData('_query', array('orderId' => Mage::registry('last_real_order_id'), 'create_by' => 'reseller'));
        }
        if(Mage::registry('_store')){
             $params->setData('_store', Mage::registry('_store'));
        }
    }
    
    public function deleteCommission($observer) {
        $order = $observer->getOrder();
        $collection = Mage::getModel('reseller/commission')->getCollection();
        $collection->addFieldToFilter('increment_id', $order->getIncrementId());
        if($collection->count() > 0) {
            foreach ($collection as $item) {
                $item->delete();
            }
        }
    }
}
