<?php
// https://fishpig.co.uk/magento/tutorials/run-magento-code-externally/
require_once('../../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
// Define store ID
Mage::app()->setCurrentStore(1);
$currentstoreid = 1;
// Define root store ID
$rootstoreid = 4;
// WEC fabrics URL path
$wecfabricsurl = Mage::app()->getStore($rootstoreid)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK) . 'wec/fabrics/';
// Tympanus grid https://tympanus.net/codrops/2013/03/19/thumbnail-grid-with-expanding-preview/
// Isotope filtering example https://codepen.io/desandro/pen/GFbAs
function maxYarnValue($str){
    $rmSpace = str_replace(str_split(' '), '', strtolower(trim($str)));
    $new = explode('x', str_replace(str_split('+'), 'x', strtolower($rmSpace)));
    $group = array();
    foreach ($new as $item) {
        $group[] = (int) substr($item, 0, -2);
    }
    $newArr = array_unique($group);
    $max = max($newArr);
    return $max;
} ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tailorist shirt fabrics, made-to-measure bespoke dress shirts</title>
    <meta name="description" content="Tailorist fabrics for men's dress shirts. Over 350 fabrics from Thomas Mason, Canclini, Albini, Tessitura Monti, Luthai, James fabrics, Dechamps and more." />
    <link rel="shortcut icon" href="<?php echo $wecfabricsurl; ?>images/favicon.png">
    <link rel="stylesheet" type="text/css" href="<?php echo $wecfabricsurl; ?>css/default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $wecfabricsurl; ?>css/component.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $wecfabricsurl; ?>css/custom.css" />
    <script src="<?php echo $wecfabricsurl; ?>js/modernizr.custom.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-68435498-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-68435498-2');
    </script>
</head>

<body>
<nav class="navbar">
    <div class="container">
        <a href="/en/" title="Tailorist logo" class="logo">
            <img src="<?php echo $wecfabricsurl; ?>images/logo.png" alt="Tailorist logo" width="auto" height="50px" />
        </a>
        <ul class="navbar-list">
            <li class="navbar-item navbar-link"><a href="<?php echo $wecfabricsurl; ?>">中文</a></li>
            <li class="navbar-item navbar-link">EN</li>
        </ul>
    </div>
</nav>

<form id="form-ui">
    <?php
    // Color Info
    $name='color_info';
    $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem();
    $attributeLabel = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem()->getStoreLabel($currentstoreid);
    $attributeId = $attributeInfo->getAttributeId();
    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
    $allAttributeValue = $attribute ->getSource()->getAllOptions(false);
    $collection = Mage::getModel('catalog/layer')->getProductCollection();
    $collection = $collection->addAttributetoSelect($name)->groupByAttribute($name);
    $attributeValues = array();
    foreach ($collection as $attributeValue){
        $valueArray = explode(',',$attributeValue->getData($name));
        $attributeValues = array_unique(array_merge($attributeValues,$valueArray));
    }
    $availableValue = array();
    foreach ($allAttributeValue as $value){
        if(in_array($value['value'],$attributeValues)){
            $availableValue[] =  $value;
        }
    }
    $attributeOptions = $availableValue;
    echo '<div class="select-group">';
    echo '<select id="' . $name . '" class="item-filter-select" data-filter-group="' . $name . '">';
    echo '<option value="" selected="selected">' . $attributeLabel . '</option>';
    foreach ($attributeOptions as $attributeOption):
        echo '<option value=".' . str_replace(' ', '', strtolower($attributeOption['label'])) . '">' . $attributeOption['label'] . '</option>';
    endforeach;
    echo '</select></div>';

    // Composition
    $name='composition';
    $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem();
    $attributeLabel = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem()->getStoreLabel($currentstoreid);
    $attributeId = $attributeInfo->getAttributeId();
    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
    $allAttributeValue = $attribute ->getSource()->getAllOptions(false);
    $collection = Mage::getModel('catalog/product')->getCollection();
    $collection = $collection->addAttributetoSelect($name)->groupByAttribute($name);
    $attributeValues = array();
    foreach ($collection as $attributeValue){
        $valueArray = explode(',',$attributeValue->getData($name));
        $attributeValues = array_unique(array_merge($attributeValues,$valueArray));
    }
    $availableValue = array();
    foreach ($allAttributeValue as $value){
        if(in_array($value['value'],$attributeValues)){
            $availableValue[] =  $value;
        }
    }
    $attributeOptions = $availableValue;
    echo '<div class="select-group">';
    echo '<select id="' . $name . '" class="item-filter-select" data-filter-group="' . $name . '">';
    echo '<option value="" selected="selected">' . $attributeLabel . '</option>';
    foreach ($attributeOptions as $attributeOption):
        echo '<option value=".c' . str_replace(str_split(' %/'), '', strtolower($attributeOption['label'])) . '">' . $attributeOption['label'] . '</option>';
    endforeach;
    echo '</select></div>';

    // Pattern
    $name='pattern';
    $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem();
    $attributeLabel = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem()->getStoreLabel($currentstoreid);
    $attributeId = $attributeInfo->getAttributeId();
    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
    $allAttributeValue = $attribute ->getSource()->getAllOptions(false);
    $collection = Mage::getModel('catalog/layer')->getProductCollection();
    $collection = $collection->addAttributetoSelect($name)->groupByAttribute($name);
    $attributeValues = array();
    foreach ($collection as $attributeValue){
        $valueArray = explode(',',$attributeValue->getData($name));
        $attributeValues = array_unique(array_merge($attributeValues,$valueArray));
    }
    $availableValue = array();
    foreach ($allAttributeValue as $value){
        if(in_array($value['value'],$attributeValues)){
            $availableValue[] =  $value;
        }
    }
    $attributeOptions = $availableValue;
    echo '<div class="select-group">';
    echo '<select id="' . $name . '" class="item-filter-select" data-filter-group="' . $name . '">';
    echo '<option value="" selected="selected">' . $attributeLabel . '</option>';
    foreach ($attributeOptions as $attributeOption):
        echo '<option value=".' . str_replace(' ', '', strtolower($attributeOption['label'])) . '">' . $attributeOption['label'] . '</option>';
    endforeach;
    echo '</select></div>';

    // Weaving
    $name='weaving';
    $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem();
    $attributeLabel = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem()->getStoreLabel($currentstoreid);
    $attributeId = $attributeInfo->getAttributeId();
    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
    $allAttributeValue = $attribute ->getSource()->getAllOptions(false);
    $collection = Mage::getModel('catalog/layer')->getProductCollection();
    $collection = $collection->addAttributetoSelect($name)->groupByAttribute($name);
    //Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
    $attributeValues = array();
    foreach ($collection as $attributeValue){
        $valueArray = explode(',',$attributeValue->getData($name));
        $attributeValues = array_unique(array_merge($attributeValues,$valueArray));
    }
    $availableValue = array();
    foreach ($allAttributeValue as $value){
        if(in_array($value['value'],$attributeValues)){
            $availableValue[] =  $value;
        }
    }
    $attributeOptions = $availableValue;
    echo '<div class="select-group">';
    echo '<select id="' . $name . '" class="item-filter-select" data-filter-group="' . $name . '">';
    echo '<option value="" selected="selected">' . $attributeLabel . '</option>';
    foreach ($attributeOptions as $attributeOption):
        echo '<option value=".' . str_replace(' ', '', strtolower($attributeOption['label'])) . '">' . $attributeOption['label'] . '</option>';
    endforeach;
    echo '</select></div>';

    // Yarn
    $name='yarn';
    $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem();
    $attributeLabel = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem()->getStoreLabel($currentstoreid);
    $attributeId = $attributeInfo->getAttributeId();
    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
    $allAttributeValue = $attribute ->getSource()->getAllOptions(false);
    $collection = Mage::getModel('catalog/product')->getCollection();
    $collection = $collection->addAttributetoSelect($name)->groupByAttribute($name);
    $attributeValues = array();
    foreach ($collection as $attributeValue){
        $valueArray = explode(',',$attributeValue->getData($name));
        $attributeValues = array_unique(array_merge($attributeValues,$valueArray));
    }
    $availableValue = array();
    foreach ($allAttributeValue as $value){
        if(in_array($value['value'],$attributeValues)){
            $availableValue[] =  $value;
        }
    }
    $attributeOptions = $availableValue;
    echo '<div class="select-group" data-filter-group="' . $name . '">';
    echo '<select id="' . $name . '" class="item-filter-select" data-filter-group="' . $name . '">';
        echo '<option value="" selected="selected">' . $attributeLabel . '</option>';
        echo '<option value=".below80">Below 80s</option>';
        echo '<option value=".80">80s</option>';
        echo '<option value=".100">100s</option>';
        echo '<option value=".120">120s</option>';
        echo '<option value=".over140" data-filter-min="140">140s +</option>';
    echo '</select></div>';

    // Manufacturer
    $name='manufacturer';
    $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem();
    $attributeLabel = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem()->getStoreLabel($currentstoreid);
    $attributeId = $attributeInfo->getAttributeId();
    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
    $allAttributeValue = $attribute ->getSource()->getAllOptions(false);
    $collection = Mage::getModel('catalog/layer')->getProductCollection();
    $collection = $collection->addAttributetoSelect($name)->groupByAttribute($name);
    $attributeValues = array();
    foreach ($collection as $attributeValue){
        $valueArray = explode(',',$attributeValue->getData($name));
        $attributeValues = array_unique(array_merge($attributeValues,$valueArray));
    }
    $availableValue = array();
    foreach ($allAttributeValue as $value){
        if(in_array($value['value'],$attributeValues)){
            $availableValue[] =  $value;
        }
    }
    $attributeOptions = $availableValue;
    echo '<div class="select-group">';
    echo '<select id="' . $name . '" class="item-filter-select" data-filter-group="' . $name . '">';
    echo '<option value="" selected="selected">' . $attributeLabel . '</option>';
    foreach ($attributeOptions as $attributeOption):
        echo '<option value=".' . str_replace(str_split(' %;-+'), '', strtolower($attributeOption['label'])) . '">' . $attributeOption['label'] . '</option>';
    endforeach;
    echo '</select></div>';
	
	
	// Treatment
	 $name='treatment';
    $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem();
    $attributeLabel = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem()->getStoreLabel($currentstoreid);
    $attributeId = $attributeInfo->getAttributeId();
    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
    $allAttributeValue = $attribute ->getSource()->getAllOptions(false);
    $collection = Mage::getModel('catalog/product')->getCollection();
    $collection = $collection->addAttributetoSelect($name)->groupByAttribute($name);
    $attributeValues = array();
    foreach ($collection as $attributeValue){
        $valueArray = explode(',',$attributeValue->getData($name));
        $attributeValues = array_unique(array_merge($attributeValues,$valueArray));
    }
    $availableValue = array();
    foreach ($allAttributeValue as $value){
        if(in_array($value['value'],$attributeValues)){
            $availableValue[] =  $value;
        }
    }
    $attributeOptions = $availableValue;
    echo '<div class="select-group" data-filter-group="' . $name . '">';
    echo '<select id="' . $name . '" class="item-filter-select" data-filter-group="' . $name . '">';
        echo '<option value="" selected="selected">' . $attributeLabel . '</option>';
        echo '<option value=".treatment_1">Wrinkle-free</option>';
        echo '<option value=".treatment_2">Easy to iron</option>';
        echo '<option value=".treatment_3">Stretch finishing</option>';
        echo '<option value=".treatment_4">All the rest</option>';
    echo '</select></div>';
    ?>
    <div class="select-group">
        <span id="search-form">
        	<button type="button" id="btnsearch" title="Search"></button>
	        <div id="search-box" class="bubble">
	            <div class="search"><input type="text" name="search" placeholder="Search fabrics" id="quicksearch"></div>
	        </div>
    	</span>
        <button type="button" id="reset" title="Reset"></button>
    </div>
</form>

<div class="fabriccontainer">
    <div class="main">
        <?php
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToFilter('type_id', 'fabric')
            ->addAttributeToSelect('*') //or just the attributes you need
            //->addAttributeToSelect('name')
            //->addAttributeToSelect('image');
            ->addAttributeToFilter(
                'status',
                array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            );
        $collection->getSelect()->joinLeft(array('link_table' => $collection->getTable('catalog/product_super_link')),
            'link_table.product_id = e.entity_id',
            array('product_id')
        );
        $collection->getSelect()->where('link_table.product_id IS NULL');
        ?>
        <div class="message">
            <h3>We currently do not have any available fabrics for these filter combinations.</h3>
        </div>
        <ul id="og-grid" class="og-grid">
            <?php
            foreach ($collection as $product) {
                $_product = Mage::getModel('catalog/product')->load($product->getId());
                // Attributes
                $composition_label = $_product->getResource()->getAttribute('composition')->getStoreLabel();
                $composition_value = $_product->getAttributeText('composition');
                $yarn_label = $_product->getResource()->getAttribute('yarn')->getStoreLabel();
                $yarn_value = $_product->getAttributeText('yarn');
                $colorinfo_label = $_product->getResource()->getAttribute('color_info')->getStoreLabel();
                $colorinfo_value = "";
                if (is_array($_product->getAttributeText('color_info'))):
                    foreach ($_product->getAttributeText('color_info') as $color):
                        $colorinfo_value .= '<span class=\'colorinfo-value\' style=\'background-color:' . str_replace(' ', '', strtolower($color)) . '\'></span>';
                    endforeach;
                else:
                    $colorinfo_value .= '<span class=\'colorinfo-value\' style=\'background-color:' . str_replace(' ', '', strtolower($_product->getAttributeText('color_info'))) . '\'></span>';
                endif;
                $colorinfo_isotope = "";
                if (is_array($_product->getAttributeText('color_info'))):
                    foreach ($_product->getAttributeText('color_info') as $color):
                        $colorinfo_isotope .= str_replace(' ', '', strtolower($color)) . ' ';
                    endforeach;
                else:
                    $colorinfo_isotope .= str_replace(' ', '', strtolower($_product->getAttributeText('color_info'))) . ' ';
                endif;
                $weaving_label = $_product->getResource()->getAttribute('weaving')->getStoreLabel();
                $weaving_value = $_product->getAttributeText('weaving');
                $sku_label = $_product->getResource()->getAttribute('sku')->getStoreLabel();
                $sku_value = $_product->getSku();
                $pattern_label = $_product->getResource()->getAttribute('pattern')->getStoreLabel();
                $pattern_value = $_product->getAttributeText('pattern');
                $treatment_label = $_product->getResource()->getAttribute('treatment')->getStoreLabel();
                $treatment_value = $_product->getAttributeText('treatment');
				$treatment_value_final = "";
				$treatment_formatted_value = str_replace(str_split(' %/'), '', strtolower($treatment_value));
				
				

			
				$treatment_complete_string = $_product->getAttributeText('treatment');				
				$treatment_complete_string_split = explode(" ", $treatment_complete_string);
				$treatment_complete_string_final = $treatment_complete_string_split[count($treatment_complete_string_split)-1];
				if($treatment_complete_string_final == "A"){
					$treatment_value_final = "treatment_1";
				}elseif($treatment_complete_string_final == "B"){
					$treatment_value_final = "treatment_2";
				}elseif($treatment_complete_string_final == "C"){
					$treatment_value_final = "treatment_3";
				}elseif($treatment_complete_string_final == "D"){
					$treatment_value_final = "treatment_4";
					
				}else{
					
					$treatment_value_final = " ";
				}

                $shirt_weight_label = $_product->getResource()->getAttribute('shirt_weight')->getStoreLabel();
                $shirt_weight_value = $_product->getResource()->getAttribute('shirt_weight')->getFrontend()->getValue($_product);
                $manufacturer_label = $_product->getResource()->getAttribute('manufacturer')->getStoreLabel();
                $manufacturer_value = $_product->getAttributeText('manufacturer');
                $price_label = $_product->getResource()->getAttribute('price')->getStoreLabel();
                $price_value = Mage::helper('core')->currency($_product->getPrice(), true, false);
				
                echo '<li data-treatment-max="'.str_replace(str_split(' %/'), '', strtolower($treatment_value)).'" data-max="'.maxYarnValue($yarn_value).'" class="item ' . $colorinfo_value . ' ' . Mage::helper('core')->currency($product->getPrice(), false, false) . ' ' . $colorinfo_isotope . ' c' . str_replace(str_split(' %/'), '', strtolower($composition_value)) . ' ' . $treatment_value_final .' '. str_replace(' ', '', strtolower($weaving_value)) . ' ' . str_replace(' ', '', strtolower($pattern_value)) . ' ' . str_replace(' ', '', strtolower($manufacturer_value)) . ' ' . str_replace(str_split(' %;-+/'), '', strtolower($yarn_value)) . ' ' . maxYarnValue($yarn_value) . '"><a href="" data-largesrc="' . Mage::app()->getStore($rootstoreid)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'shirts/' . $product->getSku() . '/fabric/large.jpg" data-title="' . $product->getName() . '" 
                    data-composition="<span>' . $composition_label . '</span>' . $composition_value . '" 
                    data-yarn="<span>' . $yarn_label . '</span>' . $yarn_value . '" 
                    data-colorinfo="<span>' . $colorinfo_label . '</span>' . $colorinfo_value . '" 
                    data-weaving="<span>' . $weaving_label . '</span>' . $weaving_value . '" 
                    data-sku="<span>' . $sku_label . '</span>' . $sku_value . '" 
                    data-pattern="<span>' . $pattern_label . '</span>' . $pattern_value . '" 
                    data-treatment="<span>' . $treatment_label . '</span>' . $treatment_value . '" 
                    data-shirt_weight="<span>' . $shirt_weight_label . '</span>' . $shirt_weight_value . 'g/m<sup>2</sup>" 
                    data-manufacturer="<span>' . $manufacturer_label . '</span>' . $manufacturer_value . '" 
                    data-price="<span>' . $price_label . '</span>' . $price_value . '" >';
                echo '<img class="grid-img-medium lazyload" data-original="' . Mage::app()->getStore($rootstoreid)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'shirts/' . $product->getSku() . '/fabric/medium.jpg" src="'.Mage::app()->getStore($rootstoreid)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'loading.png'.'" alt="' . $product->getName() . ' (' . $product->getID() . ')" />';
                echo '<h3 title="' . $product->getName() . '">' . $product->getName() . '</h3><p>' . Mage::helper('core')->currency($product->getPrice(), true, false) . '</p></a></li>';
            } ?>
        </ul>
    </div>
</div><!-- /container -->
<script src="<?php echo $wecfabricsurl; ?>js/jquery.min.js"></script>
<script src="<?php echo $wecfabricsurl; ?>js/grid.min.js"></script>
<script>
    $(function () {
        Grid.init();
    });
</script>
<script src="<?php echo $wecfabricsurl; ?>js/isotope.pkgd.min.js"></script>
<script  src="<?php echo $wecfabricsurl; ?>js/isotope.min.js"></script>
<script src="<?php echo $wecfabricsurl; ?>js/popup.js"></script>
<script src="<?php echo $wecfabricsurl; ?>js/resetbutton.js"></script>
<script src="<?php echo $wecfabricsurl; ?>js/lazyload.min.js"></script>
<script src="<?php echo $wecfabricsurl; ?>js/withinviewport.js"></script>
<script src="<?php echo $wecfabricsurl; ?>js/jquery.withinviewport.js"></script>
<script>
    jQuery(document).ready(function ($) {
            $(window).bind("scroll", function() {
                $('img')
                .removeClass('within-viewport')
                .filter(':within-viewport')
                .addClass('within-viewport');
                $("li.item").each(function(){
                    if($(this).css("display")=="inline-block"){
                        if($(this).find('img').hasClass("within-viewport") && $(this).find('img').attr("src")=='https://www.tailorist.cn/media/loading.png'){
                            $(this).find('img').attr('src',$(this).find('img').attr('data-original'));
                        }
                    }
                });
            });
            var $win = $(window);
            var $imgs = $("img.lazyload");
            $imgs.lazyload({
                effect: "fadeIn",
                failure_limit: Math.max($imgs.length - 1, 0),
                event: 'lazylazy'
            });
        function loadVisible($els, trigger) {
            $els.filter(function () {
                var rect = this.getBoundingClientRect();
                return rect.top >= 0 && rect.top <= window.innerHeight;
            }).trigger(trigger);
        }
        function checkDom(this1) {
            $("li.item"+this1).each(function(){
                if($(this).css("display")=="inline-block"){
                    $(this).find('img').attr('src',$(this).find('img').attr('data-original'));
                }
            });
            if (!$(".item:visible").length) {
                $(".message").css("display", "block");
            } else {
                $(".message").css("display", "none");
            }
			var height = $("#og-grid").height();
			console.log(height);
			if(height == 0){
				console.log(height);
				$(".message").css("display", "block");
			}
        }
        $(".item-filter-select").on("change", function () {
            checkDom($(this).val());     
        });
    });
</script>
</body>
</html>